[![pipeline status](https://gitlab.cern.ch/cloud-infrastructure/collectd-exec-compute/badges/master/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/collectd-exec-compute/-/commits/master)

# collectd-exec-compute

A collectd plugin written in bash to gather metrics from computer nodes.

The main purpose of this plugin is to send values locally so we can trigger alarms on those if they don't fit the specs.

It gathers the following information:
  - cpus
  - sockets
  - cores
  - threads
  - 2M hugepages
  - virt_enabled
  - kvm_exist
  - kvm_accessible
  - vhost_net_exists
  - net_tun_exists


## Configuration

And example of configuration file can be found in `config/`. For instance:

```
LoadPlugin exec
<Plugin exec>
  Exec "nobody:nogroup" "/usr/bin/collectd-exec-compute.sh"
</Plugin>
```
