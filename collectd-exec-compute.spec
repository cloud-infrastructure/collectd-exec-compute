
%global libvirt_version  3.9.0

Name:           collectd-exec-compute
Version:        1.1.12
Release:        1%{?dist}
Summary:        Simple collectd exec plugin to retrieve metrics from a compute node
License:        GPLv3
Group:          Development/Libraries
BuildArch:      noarch
Url:            https://gitlab.cern.ch/cloud-infrastructure/collectd-exec-compute

Source0:        %{name}-%{version}.tar.gz

Requires:       collectd
Requires:       libvirt-client >= %{libvirt_version}
Requires:       util-linux
Requires:       lshw

Requires(post):   policycoreutils, collectd
Requires(preun):  policycoreutils, collectd
Requires(postun): policycoreutils

BuildRequires:  selinux-policy
BuildRequires:  selinux-policy-devel
BuildRequires:  selinux-policy-targeted
BuildRequires:  checkpolicy

%{?systemd_requires}
BuildRequires: systemd

%description
Simple collectd exec plugin that retrieves metrics from a compute node if 
like cpus, sockets, cores, threads and 2M hugepages. It also fecthes the
status of the hypervisor if it is able to host QEMU virtual machines through
virt-host-validate.

%prep
%setup -q -n %{name}-%{version}

%build
%install
install -p -D -m 755 src/collectd-exec-compute.sh %{buildroot}%{_bindir}/collectd-exec-compute.sh
install -p -D -m 755 src/collectd-exec-compute.sh %{buildroot}%{_sbindir}/collectd-exec-compute.sh

# selinux
make -f %{_datadir}/selinux/devel/Makefile collectd_exec_compute.pp src/collectd_exec_compute.te
install -p -m 644 -D collectd_exec_compute.pp %{buildroot}%{_datadir}/selinux/packages/%{name}/collectd_exec_compute.pp

%post
if [ "$1" -le "1" ] ; then # First install
semodule -i %{_datadir}/selinux/packages/%{name}/collectd_exec_compute.pp 2>/dev/null || :
fi
%systemd_post collectd.service

%preun
if [ "$1" -lt "1" ] ; then # Final removal
semodule -r collectd_exec_compute 2>/dev/null || :
fi
%systemd_preun collectd.service

%postun
if [ "$1" -ge "1" ] ; then # Upgrade
semodule -i %{_datadir}/selinux/packages/%{name}/collectd_exec_compute.pp 2>/dev/null || :
fi
%systemd_postun_with_restart collectd.service

%files
%attr(0755, root, root) %{_bindir}/collectd-exec-compute.sh
%attr(0755, root, root) %{_sbindir}/collectd-exec-compute.sh
%attr(0644, root, root) %{_datadir}/selinux/packages/%{name}/collectd_exec_compute.pp
%doc src/README.md

%changelog
* Mon Dec 16 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 1.1.12-1
-  Fix virt-host-validate parsing for recent versions

* Wed Dec 04 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 1.1.11-1
- [OS-18418] Report CPU scaling percentage value
- [OS-18418] Make precise CPU count regex match

* Mon Dec 18 2023 Daniel Failing <daniel.failing@cern.ch> 1.1.10-1
- network_speed: add detection of interface wiht OVN

* Fri Nov 10 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 1.1.9-1
- Add nova_partition_missing metric

* Thu Feb 16 2023 Spyridon Trigazis <spyridon.trigazis@cern.ch> 1.1.8-1
- network_speed: exclude empty bridges

* Wed Jan 18 2023 Ulrich Schwickerath <ulrich.schwickerath@cern.ch> 1.1.7-2
- rebuild for RHEL and ALMA 8 and 9

* Wed Nov 09 2022 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.7-1
- Fix virt_status value for ARM servers

* Fri Apr 22 2022 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.6-1
- Support servers with NVMe drives

* Tue Apr 27 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.5-2
- Handle situations where there is no raid partition

* Wed Apr 21 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.5-1
- Add metrics for raid (boot, root and var)

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.4-4
- Initial rebuild for el8s

* Thu Apr 01 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.4-3
- Enhance the l1tf metric values

* Thu Apr 01 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.4-2
- Enhance the l1tf metric to handle no config or different l1tf configurations

* Wed Mar 31 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.4-1
- Add l1tf status metric based on runtime kernel parameters

* Thu Jan 07 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.3-1
- Use lshw to fetch disk information and remove jq usage for fetching memory information

* Thu Jan 07 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.2-5
- Add lshw requirement for plugin

* Tue Jan 05 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.2-4
- Filter out caches and firmware memories

* Wed Dec 09 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.2-3
- Default memory value to 0 if the calculation fails

* Wed Dec 09 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.2-2
- Fix memory metric in compute nodes
- Duplicate executable so we can move it to sbin

* Thu Nov 26 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.2-1
- Allow to configure sleep interval

* Wed Nov 11 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.1-3
- Change logic to calculate network_speed

* Wed Nov 11 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.1-2
- Handle cases in which route reports multiple entries for default

* Thu Oct 29 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.1-1
- Add network_speed information in exec compute metrics

* Thu Apr 16 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.0-2.el7.cern
- Added metrics for disk and memory installed

* Thu Mar 05 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.0-1.el7.cern
- Change notification behavior and send values directly
- Include values for detection of KVM status
- Added selinux policy for accessing /dev/kvm

* Thu Jul 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-8.el7.cern
- Ignore lscpu execution if the execution has not complete

* Thu Jul 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-7.el7.cern
- Add message in the OKAY notification

* Thu Jul 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-6.el7.cern
- Send host in the failure/okay notification as it's required now
- Put back FAILURE notification instead of ERROR

* Mon May 06 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-5.el7.cern
- Raise ERROR notification instead of FAILURE

* Tue Aug 28 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-4.el7.cern
- Fix bug on interval

* Tue Aug 28 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-3.el7.cern
- Add argument value (-1) to disable specific checks

* Fri Aug 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-2.el7.cern
- Add unknown status to send OK notifications add start
- Add restart of collectd service on upgrade

* Fri Aug 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-1.el7.cern
- Initial release of the exec plugin
