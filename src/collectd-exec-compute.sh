#!/bin/bash
PARAM_INTERVAL=$1

HOSTNAME="${COLLECTD_HOSTNAME:-$(hostname -f)}"
INTERVAL="${PARAM_INTERVAL:-${COLLECTD_INTERVAL:-60}}"
EXEC_ONCE="${EXEC_ONCE:-0}"

function put_value() {
  local type="$1"
  local data="$2"
  echo "PUTVAL \"${HOSTNAME}/exec_compute/gauge-${type}\" interval=${INTERVAL} N:${data}"
}

while : ; do
  # Send values for cpu metrics
  cpudata=$(/usr/bin/lscpu)
  ret=$?
  # If the execution of lscpu fails, we cannot ensure that the data is correct so we will skip it
  if [ $ret -eq 0 ]; then
    put_value 'cpus'        "$(echo "$cpudata" | grep -Ew '^CPU\(s\):'               | cut -d':' -f2 | sed 's/ //g')"
    put_value 'cpu_scaling' "$(echo "$cpudata" | grep -Po '(?<=^CPU\(s\) scaling MHz:).*(?=%)'       | sed 's/ //g')"
    put_value 'sockets'     "$(echo "$cpudata" | grep -Ew '^Socket\(s\)'             | cut -d':' -f2 | sed 's/ //g')"
    put_value 'cores'       "$(echo "$cpudata" | grep -Ew '^Core\(s\)\ per\ socket'  | cut -d':' -f2 | sed 's/ //g')"
    put_value 'threads'     "$(echo "$cpudata" | grep -Ew '^Thread\(s\)\ per\ core'  | cut -d':' -f2 | sed 's/ //g')"

    # Send notifications for hugepages metrics
    file2M="/sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages"
    put_value 'huge2048k' "$([ -f $file2M ] && cat $file2M || echo "0")"
  fi

  # Calculate virt_status for host validation
  validatedata=$(virt-host-validate qemu)

  if [ "$(uname -p)" != "aarch64" ]; then
    virt_enabled=$(echo "$validatedata" | grep -Ew '^\s\sQEMU:\sChecking\sfor\shardware\svirtualization'        | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)
  else
    # This flag is not present on ARM nodes
    virt_enabled=1
  fi
  kvm_exist=$(echo "$validatedata"        | grep -Ew '^\s\sQEMU:\sChecking.*device.*/dev\/kvm.*exists'          | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)
  kvm_accessible=$(echo "$validatedata"   | grep -Ew '^\s\sQEMU:\sChecking\sif\sdevice.*/dev/kvm.*accessible'   | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)
  vhost_net_exists=$(echo "$validatedata" | grep -Ew '^\s\sQEMU:\sChecking\sif\sdevice.*/dev/vhost-net.*exists' | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)
  net_tun_exists=$(echo "$validatedata"   | grep -Ew '^\s\sQEMU:\sChecking\sif\sdevice.*/dev/net/tun.*exists'   | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)

  # Send just one value
  put_value 'virt_status' "$([ "$virt_enabled" -eq 1 ] && [ "$kvm_exist" -eq 1 ] && [ "$kvm_accessible" -eq 1 ] && [ "$vhost_net_exists" -eq 1 ] && [ "$vhost_net_exists" -eq 1 ] && [ "$net_tun_exists" -eq 1 ] && echo 1 || echo 0)"

  # Fetch Hardware data from the node
  HW_DATA=$(lshw 2>/dev/null)

  # Calculate disks installed
  put_value 'disks' "$(echo "$HW_DATA" | grep -cE '\*-nvme:*|\*-disk:*')"

  # Calculate memory installed
  MEMORY_IN_GB=$(echo "$HW_DATA" | grep -e '\*-memory:*' -A 6 | grep -Ew '\s*size:' | cut -d':' -f2 | sed 's/[^0-9]*//g')
  put_value 'memory' "${MEMORY_IN_GB:-0}"

  # Calculate network speed
  network_adapter=""
  if [ -x /usr/sbin/brctl ]; then
    # LinuxBridge Setup
    network_adapter=$(brctl show | grep -v 'bridge name' | grep -v nic | grep -v tap | grep -v 8000.000000000000 | cut -f6)
  else
    # Otherwise check all physical interfaces, that are up
    network_adapter=$(find /sys/class/net/*/ -maxdepth 1 -name device | cut -d '/' -f5 | xargs -I {} grep -H  up /sys/class/net/{}/operstate | cut -d '/' -f5)
  fi
  if [ -z "$network_adapter" ]; then
    # Fallback to the interface with default route
    network_adapter=$(route | grep '^default' | grep -o '[^ ]*$' | head -n1)
  fi
  network_speed=$(ethtool "$network_adapter" 2>/dev/null | grep Speed | cut -d: -f2 | cut -d'M' -f1)
  if ! [[ $network_speed =~ [0-9]+ ]] ; then
    network_speed=0
  fi

  put_value 'network_speed' $network_speed

  # Kernel checks (l1tf)
  l1tf_cmdline=$(grep l1tf /proc/cmdline)
  l1tf_config="${l1tf_cmdline##*l1tf=}"
  l1tf_config="${l1tf_config%% *}"

  if [ -z "$l1tf_cmdline" ]; then
    # no l1tf configuration
    l1tf_status=0
  elif [ "$l1tf_config" = "flush,nowarn" ]; then
    # l1tf=flush,nowarn
    l1tf_status=1
  elif [ "$l1tf_config" = "full" ]; then
    # l1tf=full
    l1tf_status=2
  else
    l1tf_status=-1
  fi

  put_value 'l1tf_status' $l1tf_status

  # Identify mount points for root, var and boot
  mount_root=$(mount | grep 'on / type')
  mount_var=$(mount | grep 'on /var type')
  if [ -z "$mount_var" ]; then
    mount_var=$(mount | grep 'on /var/lib/nova type')
  fi
  mount_boot=$(mount | grep 'on /boot type')
  if [ -z "$mount_boot" ]; then
    mount_boot=$(mount | grep 'on /boot/efi type')
  fi
  if [ -z "$mount_boot" ]; then
    mount_boot=$mount_root
  fi

  # Fetch the device from the mountpoint entry
  device_root="${mount_root##/dev/}"
  device_root="${device_root%% *}"
  device_var="${mount_var##/dev/}"
  device_var="${device_var%% *}"
  device_boot="${mount_boot##/dev/}"
  device_boot="${device_boot%% *}"
 
  # Remove any subpartitioning
  device_root="${device_root%%p*}"
  device_var="${device_var%%p*}"
  device_boot="${device_boot%%p*}"

  # Get the raid mode from mdstat
  raid_root=$(grep "${device_root}" /proc/mdstat | cut -d' ' -f 4)
  raid_var=$(grep  "${device_var}"  /proc/mdstat | cut -d' ' -f 4)
  raid_boot=$(grep "${device_boot}" /proc/mdstat | cut -d' ' -f 4)

  put_value 'nova_partition_missing' "$([ -z "$mount_var" ] && echo "-1" || echo 0 )"
  put_value 'raid_root' "$([ -z "$raid_root" ] && echo "-1" || echo "${raid_root##raid}")"
  put_value 'raid_var'  "$([ -z "$raid_var" ] && echo "-1" || echo "${raid_var##raid}")"
  put_value 'raid_boot' "$([ -z "$raid_boot" ] && echo "-1" || echo "${raid_boot##raid}")"

  if [ "$EXEC_ONCE" -eq 1 ]; then
    break
  fi
  sleep "$INTERVAL"
done

exit 0
